export GHUSER="nijathasanli"
fluxctl install \
--git-user=${GHUSER} \
--git-email=${GHUSER}@users.noreply.github.com \
--git-url=git@gitlab.com:${GHUSER}/helm-opertor-get-started \
--git-path=namespaces,workloads \
--namespace=flux | kubectl apply -f -